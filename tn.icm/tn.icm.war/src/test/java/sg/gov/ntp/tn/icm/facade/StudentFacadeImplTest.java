package sg.gov.ntp.tn.icm.facade;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import tn.icm.business.service.StudentService;
import tn.icm.lib.Student;

@RunWith(MockitoJUnitRunner.Silent.class)
@WebAppConfiguration
public class StudentFacadeImplTest {

  @Mock
  StudentService studentService;

  @InjectMocks
  StudentFacadeImpl studentFacadeImpl;

  private Student student;

  @Before
  public void setUp() {
    MockitoAnnotations.initMocks(this);

    student = new Student();
    student.setStudentId(new BigDecimal(1));
    student.setFname("test");
    student.setLname("user");
    student.setAge(new BigDecimal(24));
    student.setGender("m");
    student.setCreatedBy("create");
    student.setCreatedDt(null);
    student.setLastUpdatedBy("update");
    student.setLastUpdatedDt(null);
    student.setServerName("server");
    student.setSystemName("system");
    student.setVerNo(null);
  }

  @Test
  public void saveStudent() throws Exception {


    studentFacadeImpl.saveStudent(student);

    Mockito.when(studentService.create(Mockito.any(Student.class)))
        .thenReturn(student);

    studentFacadeImpl.saveStudent(Mockito.any(Student.class));
    Mockito.verify(studentService).create(Mockito.any(Student.class));
  }

  @Test
  public void getStudent() throws Exception {

   studentFacadeImpl.getStudent(new BigDecimal(1));

    Mockito.when(studentService.findById(Mockito.any(BigDecimal.class)))
        .thenReturn(student);

    studentFacadeImpl.getStudent(Mockito.any(BigDecimal.class));
    Mockito.verify(studentService).findById(Mockito.any(BigDecimal.class));
  }

}
