package sg.gov.ntp.tn.icm.facade;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sg.gov.ntp.common.facade.NTPBaseFacade;
import tn.icm.business.service.StudentService;
import tn.icm.lib.Student;

@Component
@Transactional
public class StudentFacadeImpl extends NTPBaseFacade implements StudentFacade {

  @Autowired
  private StudentService studentService;

  public Student saveStudent(Student student) {
    return studentService.create(student);
  }

  public Student getStudent(BigDecimal id) {
    return studentService.findById(id);
  }

}
