package sg.gov.ntp.tn.icm.controller;

/**
 * This is a Controller
 */

import java.math.BigDecimal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.http.MediaType;

import sg.gov.ntp.common.controller.NTPBaseController;
import sg.gov.ntp.common.dto.NTPCommonHeaderDto;
import sg.gov.ntp.tn.icm.facade.StudentFacade;
import tn.icm.lib.Student;

@Controller
public class StudentController extends NTPBaseController {

  @Autowired
  StudentFacade studentFacade;

  @RequestMapping(value = "/ntp/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<Student> saveStudent(@RequestBody Student student) {

      studentFacade.saveStudent(student);
   
    return new ResponseEntity<Student>(student, HttpStatus.OK);

  }

  @RequestMapping(value = "/ntp/student/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<Student> getStudent(@PathVariable("id") BigDecimal id) {

    Student student = studentFacade.getStudent(id);
    return new ResponseEntity<Student>(student, HttpStatus.OK);

  }

  @Override
  public NTPCommonHeaderDto populate(String transactionId, String sessionId) {
    return super.populate(transactionId, sessionId);
  }
}