package tn.icm.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import tn.icm.lib.Classroom;
import tn.icm.lib.jpa.ClassroomEntity;
import java.math.BigDecimal;
import tn.icm.business.service.ClassroomService;
import tn.icm.business.service.mapping.ClassroomServiceMapper;
import tn.icm.data.repository.jpa.ClassroomJpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of ClassroomService
 */
@Component
@Transactional
public class ClassroomServiceImpl implements ClassroomService {

  @Resource
  private ClassroomJpaRepository classroomJpaRepository;

  @Resource
  private ClassroomServiceMapper classroomServiceMapper;

  public Classroom findById(BigDecimal classroomId) {
    ClassroomEntity classroomEntity = classroomJpaRepository
        .findOne(classroomId);
    return classroomServiceMapper
        .mapClassroomEntityToClassroom(classroomEntity);
  }

  public List<Classroom> findAll() {
    Iterable<ClassroomEntity> entities = classroomJpaRepository.findAll();
    List<Classroom> beans = new ArrayList<Classroom>();
    for (ClassroomEntity classroomEntity : entities) {
      beans.add(classroomServiceMapper
          .mapClassroomEntityToClassroom(classroomEntity));
    }
    return beans;
  }

  public Classroom save(Classroom classroom) {
    return update(classroom);
  }

  public Classroom create(Classroom classroom) {
    ClassroomEntity classroomEntity = classroomJpaRepository
        .findOne(classroom.getClassroomId());
    if (classroomEntity != null) {
      throw new IllegalStateException("already.exists");
    }
    classroomEntity = new ClassroomEntity();
    classroomServiceMapper.mapClassroomToClassroomEntity(classroom,
        classroomEntity);
    ClassroomEntity classroomEntitySaved = classroomJpaRepository
        .save(classroomEntity);
    return classroomServiceMapper
        .mapClassroomEntityToClassroom(classroomEntitySaved);
  }

  public Classroom update(Classroom classroom) {
    ClassroomEntity classroomEntity = classroomJpaRepository
        .findOne(classroom.getClassroomId());
    classroomServiceMapper.mapClassroomToClassroomEntity(classroom,
        classroomEntity);
    ClassroomEntity classroomEntitySaved = classroomJpaRepository
        .save(classroomEntity);
    return classroomServiceMapper
        .mapClassroomEntityToClassroom(classroomEntitySaved);
  }

  public void delete(BigDecimal classroomId) {
    classroomJpaRepository.delete(classroomId);
  }

  public ClassroomJpaRepository getClassroomJpaRepository() {
    return classroomJpaRepository;
  }

  public void setClassroomJpaRepository(
      ClassroomJpaRepository classroomJpaRepository) {
    this.classroomJpaRepository = classroomJpaRepository;
  }

  public ClassroomServiceMapper getClassroomServiceMapper() {
    return classroomServiceMapper;
  }

  public void setClassroomServiceMapper(
      ClassroomServiceMapper classroomServiceMapper) {
    this.classroomServiceMapper = classroomServiceMapper;
  }

}
