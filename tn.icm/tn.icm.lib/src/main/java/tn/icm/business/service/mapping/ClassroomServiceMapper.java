/*
 * Created on 6 Jul 2017 ( Time 18:32:44 )
 */
package tn.icm.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import tn.icm.lib.Classroom;
import tn.icm.lib.jpa.ClassroomEntity;
import tn.icm.lib.jpa.StudentEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class ClassroomServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;

	/**
	 * Constructor.
	 */
	public ClassroomServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'ClassroomEntity' to 'Classroom'
	 * @param classroomEntity
	 */
	public Classroom mapClassroomEntityToClassroom(ClassroomEntity classroomEntity) {
		if(classroomEntity == null) {
			return null;
		}

		//--- Generic mapping
		Classroom classroom = map(classroomEntity, Classroom.class);

		//--- Link mapping ( link to Student )
		if(classroomEntity.getStudentfk() != null) {
			classroom.setStudentId(classroomEntity.getStudentfk().getStudentId());
		}
		return classroom;
	}

	/**
	 * Mapping from 'Classroom' to 'ClassroomEntity'
	 * @param classroom
	 * @param classroomEntity
	 */
	public void mapClassroomToClassroomEntity(Classroom classroom, ClassroomEntity classroomEntity) {
		if(classroom == null) {
			return;
		}

		//--- Generic mapping
		map(classroom, classroomEntity);

		//--- Link mapping ( link : classroom )
		if( hasLinkToStudent(classroom) ) {
			StudentEntity student1 = new StudentEntity();
			student1.setStudentId( classroom.getStudentId() );
			classroomEntity.setStudentfk( student1 );
		} else {
			classroomEntity.setStudentfk( null );
		}

	}

	/**
	 * Verify that Student id is valid.
	 * @param Student Student
	 * @return boolean
	 */
	private boolean hasLinkToStudent(Classroom classroom) {
		return (classroom.getStudentId() != null)? true: false;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}