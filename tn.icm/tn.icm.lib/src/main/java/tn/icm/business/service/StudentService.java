package tn.icm.business.service;

import java.util.List;
import java.math.BigDecimal;

import tn.icm.lib.Student;

/**
 * Business Service Interface for entity Student.
 */
public interface StudentService {

  /**
   * Loads an entity from the database using its Primary Key
   *
   * @param studentId
   * @return entity
   */
  Student findById(BigDecimal studentId);

  /**
   * Loads all entities.
   *
   * @return all entities
   */
  List<Student> findAll();

  /**
   * Saves the given entity in the database (create or update)
   *
   * @param entity
   * @return entity
   */
  Student save(Student entity);

  /**
   * Updates the given entity in the database
   *
   * @param entity
   * @return
   */
  Student update(Student entity);

  /**
   * Creates the given entity in the database
   *
   * @param entity
   * @return
   */
  Student create(Student entity);

  /**
   * Deletes an entity using its Primary Key
   *
   * @param studentId
   */
  void delete(BigDecimal studentId);

}
