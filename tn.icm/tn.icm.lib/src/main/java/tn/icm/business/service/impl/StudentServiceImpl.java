package tn.icm.business.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import tn.icm.lib.Student;
import tn.icm.lib.jpa.StudentEntity;
import java.math.BigDecimal;
import tn.icm.business.service.StudentService;
import tn.icm.business.service.mapping.StudentServiceMapper;
import tn.icm.data.repository.jpa.StudentJpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import sg.gov.ntp.common.service.NTPBaseService;

/**
 * Implementation of StudentService
 */
@Component
@Transactional
public class StudentServiceImpl extends NTPBaseService implements StudentService {

  @Resource
  private StudentJpaRepository studentJpaRepository;

  @Resource
  private StudentServiceMapper studentServiceMapper;

  public Student findById(BigDecimal studentId) {
    StudentEntity studentEntity = studentJpaRepository.findOne(studentId);
    return studentServiceMapper.mapStudentEntityToStudent(studentEntity);
  }

  public List<Student> findAll() {
    Iterable<StudentEntity> entities = studentJpaRepository.findAll();
    List<Student> beans = new ArrayList<Student>();
    for (StudentEntity studentEntity : entities) {
      beans.add(studentServiceMapper.mapStudentEntityToStudent(studentEntity));
    }
    return beans;
  }

  public Student save(Student student) {
    return update(student);
  }

  public Student create(Student student) {
    StudentEntity studentEntity = studentJpaRepository
        .findOne(student.getStudentId());
    if (studentEntity != null) {
      throw new IllegalStateException("already.exists");
    }
    studentEntity = new StudentEntity();
    studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
    StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);
    return studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved);
  }

  public Student update(Student student) {
    StudentEntity studentEntity = studentJpaRepository
        .findOne(student.getStudentId());
    studentServiceMapper.mapStudentToStudentEntity(student, studentEntity);
    StudentEntity studentEntitySaved = studentJpaRepository.save(studentEntity);
    return studentServiceMapper.mapStudentEntityToStudent(studentEntitySaved);
  }

  public void delete(BigDecimal studentId) {
    studentJpaRepository.delete(studentId);
  }

  public StudentJpaRepository getStudentJpaRepository() {
    return studentJpaRepository;
  }

  public void setStudentJpaRepository(
      StudentJpaRepository studentJpaRepository) {
    this.studentJpaRepository = studentJpaRepository;
  }

  public StudentServiceMapper getStudentServiceMapper() {
    return studentServiceMapper;
  }

  public void setStudentServiceMapper(
      StudentServiceMapper studentServiceMapper) {
    this.studentServiceMapper = studentServiceMapper;
  }

}
