/*
 * Created on 6 Jul 2017 ( Time 18:32:46 )
 */
package tn.icm.business.service.mapping;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;
import tn.icm.lib.Student;
import tn.icm.lib.jpa.StudentEntity;

/**
 * Mapping between entity beans and display beans.
 */
@Component
public class StudentServiceMapper extends AbstractServiceMapper {

	/**
	 * ModelMapper : bean to bean mapping library.
	 */
	private ModelMapper modelMapper;

	/**
	 * Constructor.
	 */
	public StudentServiceMapper() {
		modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
	}

	/**
	 * Mapping from 'StudentEntity' to 'Student'
	 * @param studentEntity
	 */
	public Student mapStudentEntityToStudent(StudentEntity studentEntity) {
		if(studentEntity == null) {
			return null;
		}

		//--- Generic mapping
		return map(studentEntity, Student.class);

	}

	/**
	 * Mapping from 'Student' to 'StudentEntity'
	 * @param student
	 * @param studentEntity
	 */
	public void mapStudentToStudentEntity(Student student, StudentEntity studentEntity) {
		if(student == null) {
			return;
		}

		//--- Generic mapping
		map(student, studentEntity);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected ModelMapper getModelMapper() {
		return modelMapper;
	}

	protected void setModelMapper(ModelMapper modelMapper) {
		this.modelMapper = modelMapper;
	}

}