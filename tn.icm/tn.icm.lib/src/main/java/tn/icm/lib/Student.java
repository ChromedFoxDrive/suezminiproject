/*
 * Created on 6 Jul 2017 ( Time 18:32:45 )
 */
package tn.icm.lib;

import java.io.Serializable;

import javax.validation.constraints.*;

import java.math.BigDecimal;
import java.util.Date;

public class Student implements Serializable {

  private static final long serialVersionUID = 1L;

  // ----------------------------------------------------------------------
  // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
  // ----------------------------------------------------------------------
  @NotNull
  private BigDecimal studentId;

  // ----------------------------------------------------------------------
  // ENTITY DATA FIELDS
  // ----------------------------------------------------------------------
  @NotNull
  @Size(min = 1, max = 50)
  private String fname;

  @NotNull
  @Size(min = 1, max = 50)
  private String lname;

  @NotNull
  private BigDecimal age;

  @NotNull
  @Size(min = 1, max = 2)
  private String gender;

  @Size(max = 20)
  private String createdBy;

  private Date createdDt;

  private Date lastUpdatedDt;

  @Size(max = 20)
  private String lastUpdatedBy;

  @Size(max = 20)
  private String serverName;

  @Size(max = 20)
  private String systemName;

  private Long verNo;

  // ----------------------------------------------------------------------
  // GETTER & SETTER FOR THE KEY FIELD
  // ----------------------------------------------------------------------
  public void setStudentId(BigDecimal studentId) {
    this.studentId = studentId;
  }

  public BigDecimal getStudentId() {
    return this.studentId;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR FIELDS
  // ----------------------------------------------------------------------
  public void setFname(String fname) {
    this.fname = fname;
  }

  public String getFname() {
    return this.fname;
  }

  public void setLname(String lname) {
    this.lname = lname;
  }

  public String getLname() {
    return this.lname;
  }

  public void setAge(BigDecimal age) {
    this.age = age;
  }

  public BigDecimal getAge() {
    return this.age;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getGender() {
    return this.gender;
  }

  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  public String getCreatedBy() {
    return this.createdBy;
  }

  public void setCreatedDt(Date createdDt) {
    this.createdDt = createdDt;
  }

  public Date getCreatedDt() {
    return this.createdDt;
  }

  public void setLastUpdatedDt(Date lastUpdatedDt) {
    this.lastUpdatedDt = lastUpdatedDt;
  }

  public Date getLastUpdatedDt() {
    return this.lastUpdatedDt;
  }

  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  public String getLastUpdatedBy() {
    return this.lastUpdatedBy;
  }

  public void setServerName(String serverName) {
    this.serverName = serverName;
  }

  public String getServerName() {
    return this.serverName;
  }

  public void setSystemName(String systemName) {
    this.systemName = systemName;
  }

  public String getSystemName() {
    return this.systemName;
  }

  public void setVerNo(Long verNo) {
    this.verNo = verNo;
  }

  public Long getVerNo() {
    return this.verNo;
  }

}
