/*
 * Created on 6 Jul 2017 ( Time 18:32:45 )
 */
// This Bean has a basic Primary Key (not composite)

package tn.icm.lib.jpa;

import java.io.Serializable;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import sg.gov.ntp.common.data.NTPBaseEntity;

/**
 * Persistent class for entity stored in table "STUDENT"
 *
 *
 */

@Entity
@Table(name = "STUDENT", schema = "SYSTEM")
// Define named queries here
@NamedQueries({
    @NamedQuery(name = "StudentEntity.countAll", query = "SELECT COUNT(x) FROM StudentEntity x") })
public class StudentEntity extends NTPBaseEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  // ----------------------------------------------------------------------
  // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
  // ----------------------------------------------------------------------
  @Id
  @Column(name = "STUDENT_ID", nullable = false)
  private BigDecimal studentId;

  // ----------------------------------------------------------------------
  // ENTITY DATA FIELDS
  // ----------------------------------------------------------------------
  @Column(name = "FNAME", nullable = false, length = 50)
  private String fname;

  @Column(name = "LNAME", nullable = false, length = 50)
  private String lname;

  @Column(name = "AGE", nullable = false)
  private BigDecimal age;

  @Column(name = "GENDER", nullable = false, length = 2)
  private String gender;

  @Column(name = "CREATED_BY", length = 20)
  private String createdBy;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "CREATED_DT")
  private Date createdDt;

  @Temporal(TemporalType.TIMESTAMP)
  @Column(name = "LAST_UPDATED_DT")
  private Date lastUpdatedDt;

  @Column(name = "LAST_UPDATED_BY", length = 20)
  private String lastUpdatedBy;

  @Column(name = "SERVER_NAME", length = 20)
  private String serverName;

  @Column(name = "SYSTEM_NAME", length = 20)
  private String systemName;

  @Column(name = "VER_NO")
  private Long verNo;

  // ----------------------------------------------------------------------
  // ENTITY LINKS ( RELATIONSHIP )
  // ----------------------------------------------------------------------
  @OneToMany(mappedBy = "studentfk", targetEntity = ClassroomEntity.class)
  private List<ClassroomEntity> listOfClassroomfk;

  // ----------------------------------------------------------------------
  // CONSTRUCTOR(S)
  // ----------------------------------------------------------------------
  public StudentEntity() {
    super();
  }

  // ----------------------------------------------------------------------
  // GETTER & SETTER FOR THE KEY FIELD
  // ----------------------------------------------------------------------
  public void setStudentId(BigDecimal studentIdEntity) {
	    this.studentId = studentIdEntity;
  }

  public BigDecimal getStudentId() {
	    return this.studentId;
  }
  
  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR FIELDS
  // ----------------------------------------------------------------------
  // --- DATABASE MAPPING : FNAME ( VARCHAR2 )
  public void setFname(String fnameEntity) {
    this.fname = fnameEntity;
  }

  public String getFname() {
    return this.fname;
  }

  // --- DATABASE MAPPING : LNAME ( VARCHAR2 )
  public void setLname(String lnameEntity) {
    this.lname = lnameEntity;
  }

  public String getLname() {
    return this.lname;
  }

  // --- DATABASE MAPPING : AGE ( NUMBER )
  public void setAge(BigDecimal ageEntity) {
    this.age = ageEntity;
  }

  public BigDecimal getAge() {
    return this.age;
  }

  // --- DATABASE MAPPING : GENDER ( VARCHAR2 )
  public void setGender(String genderEntity) {
    this.gender = genderEntity;
  }

  public String getGender() {
    return this.gender;
  }

  // --- DATABASE MAPPING : CREATED_BY ( VARCHAR2 )
  @Override
  public void setCreatedBy(String createdBy) {
    this.createdBy = createdBy;
  }

  @Override
  public String getCreatedBy() {
    return this.createdBy;
  }

  // --- DATABASE MAPPING : CREATED_DT ( DATE )
  @Override
  public void setCreatedDt(Date createdDt) {
    this.createdDt = createdDt;
  }

  @Override
  public Date getCreatedDt() {
    return this.createdDt;
  }

  // --- DATABASE MAPPING : LAST_UPDATED_DT ( DATE )
  @Override
  public void setLastUpdatedDt(Date lastUpdatedDt) {
    this.lastUpdatedDt = lastUpdatedDt;
  }

  @Override
  public Date getLastUpdatedDt() {
    return this.lastUpdatedDt;
  }

  // --- DATABASE MAPPING : LAST_UPDATED_BY ( VARCHAR2 )
  @Override
  public void setLastUpdatedBy(String lastUpdatedBy) {
    this.lastUpdatedBy = lastUpdatedBy;
  }

  @Override
  public String getLastUpdatedBy() {
    return this.lastUpdatedBy;
  }

  // --- DATABASE MAPPING : SERVER_NAME ( VARCHAR2 )
  @Override
  public void setServerName(String serverName) {
    this.serverName = serverName;
  }

  @Override
  public String getServerName() {
    return this.serverName;
  }

  // --- DATABASE MAPPING : SYSTEM_NAME ( VARCHAR2 )
  @Override
  public void setSystemName(String systemName) {
    this.systemName = systemName;
  }

  @Override
  public String getSystemName() {
    return this.systemName;
  }

  // --- DATABASE MAPPING : VER_NO ( NUMBER )
  @Override
  public void setVerNo(Long verNo) {
    this.verNo = verNo;
  }

  @Override
  public Long getVerNo() {
    return this.verNo;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR LINKS
  // ----------------------------------------------------------------------
  public void setListOfClassroomfk(List<ClassroomEntity> listOfClassroomfk) {
    this.listOfClassroomfk = listOfClassroomfk;
  }

  public List<ClassroomEntity> getListOfClassroomfk() {
    return this.listOfClassroomfk;
  }

}
