/*
 * Created on 6 Jul 2017 ( Time 18:32:43 )
 */
// This Bean has a basic Primary Key (not composite)

package tn.icm.lib.jpa;

import java.io.Serializable;


import java.math.BigDecimal;

import javax.persistence.*;

/**
 * Persistent class for entity stored in table "CLASSROOM"
 *
 *
 */

@Entity
@Table(name = "CLASSROOM", schema = "SYSTEM")
// Define named queries here
@NamedQueries({
    @NamedQuery(name = "ClassroomEntity.countAll", query = "SELECT COUNT(x) FROM ClassroomEntity x") })
public class ClassroomEntity implements Serializable {

  private static final long serialVersionUID = 1L;

  // ----------------------------------------------------------------------
  // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
  // ----------------------------------------------------------------------
  @Id
  @Column(name = "CLASSROOM_ID", nullable = false)
  private BigDecimal classroomId;

  // ----------------------------------------------------------------------
  // ENTITY DATA FIELDS
  // ----------------------------------------------------------------------
  @Column(name = "CLASSROOM_NO", nullable = false)
  private BigDecimal classroomNo;

  // "studentId" (column "STUDENT_ID") is not defined by itself because used as
  // FK in a link

  // ----------------------------------------------------------------------
  // ENTITY LINKS ( RELATIONSHIP )
  // ----------------------------------------------------------------------
  @ManyToOne
  @JoinColumn(name = "STUDENT_ID", referencedColumnName = "STUDENT_ID")
  private StudentEntity studentfk;

  // ----------------------------------------------------------------------
  // CONSTRUCTOR(S)
  // ----------------------------------------------------------------------
  public ClassroomEntity() {
    super();
  }

  // ----------------------------------------------------------------------
  // GETTER & SETTER FOR THE KEY FIELD
  // ----------------------------------------------------------------------
  public void setClassroomId(BigDecimal classroomId) {
    this.classroomId = classroomId;
  }

  public BigDecimal getClassroomId() {
    return this.classroomId;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR FIELDS
  // ----------------------------------------------------------------------
  // --- DATABASE MAPPING : CLASSROOM_NO ( NUMBER )
  public void setClassroomNo(BigDecimal classroomNo) {
    this.classroomNo = classroomNo;
  }

  public BigDecimal getClassroomNo() {
    return this.classroomNo;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR LINKS
  // ----------------------------------------------------------------------
  public void setStudentfk(StudentEntity studentfk) {
    this.studentfk = studentfk;
  }

  public StudentEntity getStudentfk() {
    return this.studentfk;
  }

}
