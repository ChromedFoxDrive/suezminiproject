/*
 * Created on 6 Jul 2017 ( Time 18:32:43 )
 */
package tn.icm.lib;

import java.io.Serializable;

import javax.validation.constraints.*;

import java.math.BigDecimal;

public class Classroom implements Serializable {

  private static final long serialVersionUID = 1L;

  // ----------------------------------------------------------------------
  // ENTITY PRIMARY KEY ( BASED ON A SINGLE FIELD )
  // ----------------------------------------------------------------------
  @NotNull
  private BigDecimal classroomId;

  // ----------------------------------------------------------------------
  // ENTITY DATA FIELDS
  // ----------------------------------------------------------------------
  @NotNull
  private BigDecimal classroomNo;

  @NotNull
  private BigDecimal studentId;

  // ----------------------------------------------------------------------
  // GETTER & SETTER FOR THE KEY FIELD
  // ----------------------------------------------------------------------
  public void setClassroomId(BigDecimal classroomId) {
    this.classroomId = classroomId;
  }

  public BigDecimal getClassroomId() {
    return this.classroomId;
  }

  // ----------------------------------------------------------------------
  // GETTERS & SETTERS FOR FIELDS
  // ----------------------------------------------------------------------
  public void setClassroomNo(BigDecimal classroomNo) {
    this.classroomNo = classroomNo;
  }

  public BigDecimal getClassroomNo() {
    return this.classroomNo;
  }

  public void setStudentId(BigDecimal studentId) {
    this.studentId = studentId;
  }

  public BigDecimal getStudentId() {
    return this.studentId;
  }

}
