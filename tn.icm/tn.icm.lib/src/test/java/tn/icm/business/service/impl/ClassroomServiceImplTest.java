package tn.icm.business.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import tn.icm.lib.Classroom;
import tn.icm.lib.jpa.ClassroomEntity;
import java.math.BigDecimal;
import tn.icm.business.service.mapping.ClassroomServiceMapper;
import tn.icm.data.repository.jpa.ClassroomJpaRepository;
import tn.icm.test.ClassroomFactoryForTest;
import tn.icm.test.ClassroomEntityFactoryForTest;
import tn.icm.test.MockValues;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Test : Implementation of ClassroomService
 */
@RunWith(MockitoJUnitRunner.class)
public class ClassroomServiceImplTest {

	@InjectMocks
	private ClassroomServiceImpl classroomService;
	@Mock
	private ClassroomJpaRepository classroomJpaRepository;
	@Mock
	private ClassroomServiceMapper classroomServiceMapper;

	private ClassroomFactoryForTest classroomFactoryForTest = new ClassroomFactoryForTest();

	private ClassroomEntityFactoryForTest classroomEntityFactoryForTest = new ClassroomEntityFactoryForTest();

	private MockValues mockValues = new MockValues();

	@Test
	public void findById() {
		// Given
		BigDecimal classroomId = mockValues.nextBigDecimal();

		ClassroomEntity classroomEntity = classroomJpaRepository.findOne(classroomId);

		Classroom classroom = classroomFactoryForTest.newClassroom();
		when(classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntity)).thenReturn(classroom);

		// When
		Classroom classroomFound = classroomService.findById(classroomId);

		// Then
		assertEquals(classroom.getClassroomId(),classroomFound.getClassroomId());
	}

	@Test
	public void findAll() {
		// Given
		List<ClassroomEntity> classroomEntitys = new ArrayList<ClassroomEntity>();
		ClassroomEntity classroomEntity1 = classroomEntityFactoryForTest.newClassroomEntity();
		classroomEntitys.add(classroomEntity1);
		ClassroomEntity classroomEntity2 = classroomEntityFactoryForTest.newClassroomEntity();
		classroomEntitys.add(classroomEntity2);
		when(classroomJpaRepository.findAll()).thenReturn(classroomEntitys);

		Classroom classroom1 = classroomFactoryForTest.newClassroom();
		when(classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntity1)).thenReturn(classroom1);
		Classroom classroom2 = classroomFactoryForTest.newClassroom();
		when(classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntity2)).thenReturn(classroom2);

		// When
		List<Classroom> classroomsFounds = classroomService.findAll();

		// Then
		assertTrue(classroom1 == classroomsFounds.get(0));
		assertTrue(classroom2 == classroomsFounds.get(1));
	}

	@Test
	public void create() {
		// Given
		Classroom classroom = classroomFactoryForTest.newClassroom();

		ClassroomEntity classroomEntity = classroomEntityFactoryForTest.newClassroomEntity();
		when(classroomJpaRepository.findOne(classroom.getClassroomId())).thenReturn(null);

		classroomEntity = new ClassroomEntity();
		classroomServiceMapper.mapClassroomToClassroomEntity(classroom, classroomEntity);
		ClassroomEntity classroomEntitySaved = classroomJpaRepository.save(classroomEntity);

		Classroom classroomSaved = classroomFactoryForTest.newClassroom();
		when(classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntitySaved)).thenReturn(classroomSaved);

		// When
		Classroom classroomResult = classroomService.create(classroom);

		// Then
		assertTrue(classroomResult == classroomSaved);
	}

	@Test
	public void createKOExists() {
		// Given
		Classroom classroom = classroomFactoryForTest.newClassroom();

		ClassroomEntity classroomEntity = classroomEntityFactoryForTest.newClassroomEntity();
		when(classroomJpaRepository.findOne(classroom.getClassroomId())).thenReturn(classroomEntity);

		// When
		Exception exception = null;
		try {
			classroomService.create(classroom);
		} catch(Exception e) {
			exception = e;
		}

		// Then
		assertTrue(exception instanceof IllegalStateException);
		assertEquals("already.exists", exception.getMessage());
	}

	@Test
	public void update() {
		// Given
		Classroom classroom = classroomFactoryForTest.newClassroom();

		ClassroomEntity classroomEntity = classroomEntityFactoryForTest.newClassroomEntity();
		when(classroomJpaRepository.findOne(classroom.getClassroomId())).thenReturn(classroomEntity);

		ClassroomEntity classroomEntitySaved = classroomEntityFactoryForTest.newClassroomEntity();
		when(classroomJpaRepository.save(classroomEntity)).thenReturn(classroomEntitySaved);

		Classroom classroomSaved = classroomFactoryForTest.newClassroom();
		when(classroomServiceMapper.mapClassroomEntityToClassroom(classroomEntitySaved)).thenReturn(classroomSaved);

		// When
		Classroom classroomResult = classroomService.update(classroom);

		// Then
		verify(classroomServiceMapper).mapClassroomToClassroomEntity(classroom, classroomEntity);
		assertTrue(classroomResult == classroomSaved);
	}

	@Test
	public void delete() {
		// Given
		BigDecimal classroomId = mockValues.nextBigDecimal();

		// When
		classroomService.delete(classroomId);

		// Then
		verify(classroomJpaRepository).delete(classroomId);

	}

}
