package tn.icm.lib;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import tn.icm.test.MockValues;


import java.math.BigDecimal;
import java.util.Date;

public class StudentTest {

    private static Student studentTestValue = null;

    private static MockValues mockValues = new MockValues();

    private static BigDecimal studentIdInternalMockValue = mockValues.nextBigDecimal();
    private static String fnameInternalMockValue = mockValues.nextString(50);
    private static String lnameInternalMockValue = mockValues.nextString(50);
    private static BigDecimal ageInternalMockValue = mockValues.nextBigDecimal();
    private static String genderInternalMockValue = mockValues.nextString(2);
    private static String createdByInternalMockValue = mockValues.nextString(20);
    private static Date createdDtInternalMockValue = mockValues.nextDate();
    private static Date lastUpdatedDtInternalMockValue = mockValues.nextDate();
    private static String lastUpdatedByInternalMockValue = mockValues.nextString(20);
    private static String serverNameInternalMockValue = mockValues.nextString(20);
    private static String systemNameInternalMockValue = mockValues.nextString(20);
    private static BigDecimal verNoInternalMockValue = mockValues.nextBigDecimal();

    @Before
    public void setUp() throws Exception {

    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        studentTestValue = new Student();

        // Prepare test data for Getter methods.
        studentTestValue.setStudentId(studentIdInternalMockValue);
        studentTestValue.setFname(fnameInternalMockValue);
        studentTestValue.setLname(lnameInternalMockValue);
        studentTestValue.setAge(ageInternalMockValue);
        studentTestValue.setGender(genderInternalMockValue);
        studentTestValue.setCreatedBy(createdByInternalMockValue);
        studentTestValue.setCreatedDt(createdDtInternalMockValue);
        studentTestValue.setLastUpdatedDt(lastUpdatedDtInternalMockValue);
        studentTestValue.setLastUpdatedBy(lastUpdatedByInternalMockValue);
        studentTestValue.setServerName(serverNameInternalMockValue);
        studentTestValue.setSystemName(systemNameInternalMockValue);
        //studentTestValue.setVerNo(verNoInternalMockValue);
    }

    @After
    public void tearDown() throws Exception {

    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        studentTestValue = null;
        mockValues = null;
    }

    //----------------------------------------------------------------------
    // GETTERS & SETTERS FOR FIELDS
    //----------------------------------------------------------------------
    @Test
    public void testGetStudentId () {
        assertEquals(studentIdInternalMockValue, studentTestValue.getStudentId());
    }

    @Test
    public void testSetStudentId () {
        studentTestValue.setStudentId(studentIdInternalMockValue);
        assertEquals(studentIdInternalMockValue, studentTestValue.getStudentId());
    }

    @Test
    public void testGetFname () {
        assertEquals(fnameInternalMockValue, studentTestValue.getFname());
    }

    @Test
    public void testSetFname () {
        studentTestValue.setFname(fnameInternalMockValue);
        assertEquals(fnameInternalMockValue, studentTestValue.getFname());
    }

    @Test
    public void testGetLname () {
        assertEquals(lnameInternalMockValue, studentTestValue.getLname());
    }

    @Test
    public void testSetLname () {
        studentTestValue.setLname(lnameInternalMockValue);
        assertEquals(lnameInternalMockValue, studentTestValue.getLname());
    }

    @Test
    public void testGetAge () {
        assertEquals(ageInternalMockValue, studentTestValue.getAge());
    }

    @Test
    public void testSetAge () {
        studentTestValue.setAge(ageInternalMockValue);
        assertEquals(ageInternalMockValue, studentTestValue.getAge());
    }

    @Test
    public void testGetGender () {
        assertEquals(genderInternalMockValue, studentTestValue.getGender());
    }

    @Test
    public void testSetGender () {
        studentTestValue.setGender(genderInternalMockValue);
        assertEquals(genderInternalMockValue, studentTestValue.getGender());
    }

    @Test
    public void testGetCreatedBy () {
        assertEquals(createdByInternalMockValue, studentTestValue.getCreatedBy());
    }

    @Test
    public void testSetCreatedBy () {
        studentTestValue.setCreatedBy(createdByInternalMockValue);
        assertEquals(createdByInternalMockValue, studentTestValue.getCreatedBy());
    }

    @Test
    public void testGetCreatedDt () {
        assertEquals(createdDtInternalMockValue, studentTestValue.getCreatedDt());
    }

    @Test
    public void testSetCreatedDt () {
        studentTestValue.setCreatedDt(createdDtInternalMockValue);
        assertEquals(createdDtInternalMockValue, studentTestValue.getCreatedDt());
    }

    @Test
    public void testGetLastUpdatedDt () {
        assertEquals(lastUpdatedDtInternalMockValue, studentTestValue.getLastUpdatedDt());
    }

    @Test
    public void testSetLastUpdatedDt () {
        studentTestValue.setLastUpdatedDt(lastUpdatedDtInternalMockValue);
        assertEquals(lastUpdatedDtInternalMockValue, studentTestValue.getLastUpdatedDt());
    }

    @Test
    public void testGetLastUpdatedBy () {
        assertEquals(lastUpdatedByInternalMockValue, studentTestValue.getLastUpdatedBy());
    }

    @Test
    public void testSetLastUpdatedBy () {
        studentTestValue.setLastUpdatedBy(lastUpdatedByInternalMockValue);
        assertEquals(lastUpdatedByInternalMockValue, studentTestValue.getLastUpdatedBy());
    }

    @Test
    public void testGetServerName () {
        assertEquals(serverNameInternalMockValue, studentTestValue.getServerName());
    }

    @Test
    public void testSetServerName () {
        studentTestValue.setServerName(serverNameInternalMockValue);
        assertEquals(serverNameInternalMockValue, studentTestValue.getServerName());
    }

    @Test
    public void testGetSystemName () {
        assertEquals(systemNameInternalMockValue, studentTestValue.getSystemName());
    }

    @Test
    public void testSetSystemName () {
        studentTestValue.setSystemName(systemNameInternalMockValue);
        assertEquals(systemNameInternalMockValue, studentTestValue.getSystemName());
    }

/*    @Test
    public void testGetVerNo () {
        assertEquals(verNoInternalMockValue, studentTestValue.getVerNo());
    }*/

/*    @Test
    public void testSetVerNo () {
        studentTestValue.setVerNo(verNoInternalMockValue);
        assertEquals(verNoInternalMockValue, studentTestValue.getVerNo());
    }*/


}
